package com.agt.debs.rdf

import java.io.ByteArrayInputStream

import com.agt.debs.Initializer
import com.agt.debs.model.MachineModel
import com.agt.debs.util.{DateConverter, SimpleKafkaClient}
import org.apache.jena.query._
import org.apache.jena.rdf.model.{Model, ModelFactory, RDFNode}
import org.apache.jena.riot.{Lang, RDFDataMgr}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}


object EventExtractor {
  val client = new SimpleKafkaClient(Initializer.kafkaServer)
  val producer = new KafkaProducer[String, MachineModel](client.basicProducer)
  def processEvent(bytes: Array[Byte]): Unit = {
    val model: Model = ModelFactory.createDefaultModel()
    val inputStream = new ByteArrayInputStream(bytes)
    RDFDataMgr.read(model, inputStream, Lang.NTRIPLES)

    val queryString: String = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>" +
      "PREFIX IoTCore: <http://www.agtinternational.com/ontologies/IoTCore#>" +
      "PREFIX i40: <http://www.agtinternational.com/ontologies/I4.0#>" +
      "PREFIX ssn: <http://purl.oclc.org/NET/ssnx/ssn#>" +
      "SELECT ?machine ?time ?timestamp ?dimension ?value WHERE { " +
      "?observation rdf:type i40:MoldingMachineObservationGroup ." +
      "?observation i40:machine ?machine ." +
      "?observation ssn:observationResultTime ?time ." +
      "?time IoTCore:valueLiteral ?timestamp ." +
      "?observation i40:contains ?obGroup ." +
      "?obGroup ssn:observedProperty ?dimension ." +
      "?obGroup ssn:observationResult ?output ." +
      "?output ssn:hasValue ?valID ." +
      "?valID IoTCore:valueLiteral ?value ." +
      "}"
    val query: Query = QueryFactory.create(queryString)
    val qexec: QueryExecution = QueryExecutionFactory.create(query, model)

    val results: ResultSet = qexec.execSelect()
    //ResultSetFormatter.out(System.out, results)
    while (results.hasNext) {
      val soln: QuerySolution = results.nextSolution()
      val machine: RDFNode = soln.getResource("machine")
      val time: RDFNode = soln.getResource("time")
      val timeStamp: RDFNode = soln.getLiteral("timestamp")
      val dimension: RDFNode = soln.getResource("dimension")
      val value: RDFNode = soln.getLiteral("value")
      val domain: MachineModel = new MachineModel()
      val idArr: Array[String] = machine.toString.split("#")
      val timeArr: Array[String] = time.toString.split("#")
      val dimArr: Array[String] = dimension.toString.split("#")
      try {
        domain.setId(idArr(1))
        domain.setValue(value.asLiteral().getDouble)
        domain.setTime(timeArr(1))
        domain.setDimension(dimArr(1))
        domain.setTimestamp(
          DateConverter.parse(timeStamp.asLiteral().getString))

        producer.send(new ProducerRecord(domain.getId, domain.getDimension, domain))
      } catch {
        case e: NumberFormatException => None
      }
    }
  }
}
