package com.agt.debs.rdf

import java.io.ByteArrayOutputStream

import org.apache.jena.rdf.model.{Model, ModelFactory, RDFNode, Resource}
import scala.collection.JavaConversions
import scala.collection.mutable

/**
  * Created by pratik_k on 11/30/2017.
  */
object RDFFormator {
  var anamolyNo = 0
  def formRdf(machine: String, dimension: String, timestamp: String, value: Double) = {
    val subject = s"http://project-hobbit.eu/resources/debs2017#Anomaly_$anamolyNo"
    val model: Model = ModelFactory.createDefaultModel()
    val typeProperty = model.createProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")
    val machineProperty = model.createProperty("http://www.agtinternational.com/ontologies/I4.0#machine")
    val dimensionProperty = model.createProperty("http://www.agtinternational.com/ontologies/DEBSAnalyticResults#inAbnormalDimension")
    val timeProperty = model.createProperty("http://www.agtinternational.com/ontologies/DEBSAnalyticResults#hasTimeStamp")
    val probabilityProperty = model.createProperty("http://www.agtinternational.com/ontologies/DEBSAnalyticResults#hasProbabilityOfObservedAbnormalSequence")
    val rootNode: Resource
      = model.createResource(subject)
    rootNode.addLiteral(probabilityProperty, value)
    rootNode.addProperty(timeProperty, s"http://project-hobbit.eu/resources/debs2017#$timestamp")
    rootNode.addProperty(dimensionProperty, s"http://www.agtinternational.com/ontologies/WeidmullerMetadata#$dimension")
    rootNode.addProperty(machineProperty, s"http://www.agtinternational.com/ontologies/WeidmullerMetadata#$machine")
    rootNode.addProperty(typeProperty, "http://www.agtinternational.com/ontologies/DEBSAnalyticResults#Anomaly")
    anamolyNo+=1

    val bytes = new ByteArrayOutputStream()
    model.write(bytes, "N-TRIPLES")
    bytes
  }

  def getKeyValue(model: Model): java.util.Map[String, Object] = {
    val keyValueMap = mutable.HashMap[String, Object]()
    val iterator = model.listStatements(null, null, null.asInstanceOf[RDFNode])
    while (iterator.hasNext) {
      val statement = iterator.nextStatement()
      val propertyUri = statement.getPredicate.getURI
      val obj = statement.getObject
      if (obj.isLiteral) {
        val literal = obj.asLiteral()
        val datatype = literal.getDatatype
        val value = datatype.parse(literal.getLexicalForm)
        keyValueMap.put(propertyUri, value)
      }
    }
    JavaConversions.mutableMapAsJavaMap(keyValueMap)
  }


}
