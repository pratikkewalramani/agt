package com.agt.debs.model

/**
  * Created by pratik_k on 12/1/2017.
  */

object SystemParameters {
  var windowSize: Option[Int] = None
  var maxClusterIterations: Option[Int] = None
  var transitions: Option[Int] = None

  def setParameters(windowSize: Int, maxCluster: Int, transitions: Int): Unit = {
    this.windowSize = Some(windowSize)
    this.maxClusterIterations = Some(maxCluster)
    this.transitions = Some(transitions)
  }
}
