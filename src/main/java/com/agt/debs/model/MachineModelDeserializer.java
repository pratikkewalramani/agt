package com.agt.debs.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;

import java.util.Map;

/**
 * Created by pratik_k on 11/28/2017.
 */
public class MachineModelDeserializer  implements Deserializer {
    @Override
    public void close() {
    }

    @Override
    public void configure(Map map, boolean b) {

    }

    @Override
    public MachineModel deserialize(String arg0, byte[] arg1) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        MachineModel model = null;
        try {
            model = mapper.readValue(arg1, MachineModel.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return model;
    }
}
