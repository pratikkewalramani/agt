package com.agt.debs.processing

import java.lang.Math.{pow, sqrt}

import scala.annotation.tailrec
import scala.collection.mutable

case class Point(x: Double) extends Serializable{
  def distanceTo(that: Point) = sqrt(pow(this.x - that.x, 2))

  def sum(that: Point) = Point(this.x + that.x)

  def divideBy(number: Int) = Point(this.x / number)

  override def toString = s"$x"
}
/**
  * Created by pratik_k on 11/29/2017.
  */
class KMeansClustering(points:List[Point], cluster: Int, numOfIterations: Int = 50, epsilon: Double = 1e-4){
  def init(): List[Point] = {
    val centroidsMap: Map[Point, List[Point]] =  points.toList.distinct.take(cluster).map(point => (point, List.empty)).toMap
    val clusters = buildClusters(points, centroidsMap)
    var sequence:mutable.ArrayBuffer[Point] = mutable.ArrayBuffer.empty
    points.foreach(point => {
      clusters.foreach(entry => {
        if(entry._2.contains(point))
          sequence += entry._1
      })
    })
    sequence.toList
  }

  @tailrec
  private def buildClusters(points: List[Point], prevClusters: scala.collection.Map[Point, List[Point]], iteration: Int = 0): scala.collection.Map[Point, List[Point]] = {
    val nextClusters = points.map({ point =>
      val byDistanceToPoint = new Ordering[Point] {
        override def compare(p1: Point, p2: Point) = p1.distanceTo(point) compareTo p2.distanceTo(point)
      }

      (point, prevClusters.keys min byDistanceToPoint)
    }).groupBy({ case (_, centroid) => centroid })
      .map({ case (centroid, pointsToCentroids) =>
        val points = pointsToCentroids.map({ case (point, _) => point })
        (centroid, points)
      })

    // Calculate the centroid movement for the stopping condition
    val movement = (prevClusters.keys zip nextClusters.keys).map({ case (a, b) => a distanceTo b })
    if (movement.exists(_ > epsilon) && iteration < numOfIterations) {
      val nextClustersWithBetterCentroids = nextClusters.map({
        case (centroid, members) =>
          val (sum, count) = members.foldLeft((Point(0), 0))({ case ((acc, c), curr) => (acc sum curr, c + 1) })
          (sum divideBy count, members)
      })

      buildClusters(points, nextClustersWithBetterCentroids, iteration + 1)
    } else {
      nextClusters
    }
  }
}
