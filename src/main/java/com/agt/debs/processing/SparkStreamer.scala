package com.agt.debs.processing

import java.util.Properties

import com.agt.debs.model.{MachineModel, SystemParameters}
import com.agt.debs.rdf.RDFFormator
import com.agt.debs.util.{EmbeddedKafkaServer, MetaDataManager, SimpleKafkaClient}
import com.agt.debs.Initializer
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, KafkaUtils, LocationStrategies}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.JavaConverters._

/**
  * Created by pratik_k on 11/28/2017.
  */
class SparkStreamer(kafkaServer: EmbeddedKafkaServer) {
  val sparkConf = new SparkConf().setAppName("AnomalyDetectionTest").setMaster("local[*]")
  val sc = new SparkContext(sparkConf)
  sc.setLogLevel("WARN")
  val ssc = new StreamingContext(sc, Seconds(SystemParameters.windowSize.get))

  val props: Properties = SimpleKafkaClient.getBasicConsumer(kafkaServer)

  val kafkaStream =
    KafkaUtils.createDirectStream(
      ssc,
      LocationStrategies.PreferConsistent,
      ConsumerStrategies.Subscribe[String, MachineModel](
        //list of topics
        MetaDataManager.listOfMachines.asJava,
        props.asInstanceOf[java.util.Map[String, Object]]
      )
    )

  val machineStream = kafkaStream.map(tuple => (tuple.topic(), tuple.key()) -> (tuple.value().getValue.toDouble, tuple.value().getTimestamp, tuple.value().getTime))
  val windowStream = machineStream.groupByKeyAndWindow(Seconds(SystemParameters.windowSize.get))
  windowStream.foreachRDD(r => {
    r.foreach(tuple => {
      val transitions: Int = SystemParameters.transitions.get
      val machine = tuple._1._1
      val dimension = tuple._1._2
      if(MetaDataManager.dimensionMetaDataMap.contains(dimension)){
        val cluster = MetaDataManager.dimensionMetaDataMap.get(dimension).get._2.get
        //println(dimension + " cluster " + cluster)
        val tupleTimeandValue = tuple._2.map(value => (value._1, value._3))
        val valueSorted: Iterable[(Double, String)] = tupleTimeandValue.toStream.sortBy(value => value._2.split("_").apply(1).toInt)
        //valueSorted.foreach(println)

        val values = valueSorted.map(value => Point(value._1))
        val kmeans = new KMeansClustering(values.toList, cluster, SystemParameters.maxClusterIterations.get)
        val clusterPointMapping = kmeans.init()
        var chain = new MarkovChain[Double]()
        //forms the markov model
        clusterPointMapping.sliding(2,1).foreach(elems => chain = chain.addTransition(elems.head.x, elems.last.x))
        //take last n+1 elements
        val lastNElemennts = clusterPointMapping.takeRight(transitions+1)
        val probabilities = lastNElemennts.sliding(2,1).map(elem => chain.transitionProbability(elem.head.x, elem.last.x))
        val finalValue = probabilities.product
        val threshold = MetaDataManager.dimensionMetaDataMap.get(dimension).get._1.get
        if(finalValue < threshold) {
          val valMap: Map[Double, String] = valueSorted.toMap
          val triggeringElement = values.takeRight(transitions).head
          val timeStamp = valMap.get(triggeringElement.x)
          println(machine + " Anamoly " + dimension + " " + finalValue + "time " + timeStamp.get)
          val bytes = RDFFormator.formRdf(machine, dimension, timeStamp.get, finalValue)
          if(Initializer.debsGcBenchmarkSystem != null)
            Initializer.debsGcBenchmarkSystem.sendToOutputQueue(bytes.toByteArray)
        }
      }
    })
  })

  ssc.start()

  try {
    ssc.awaitTermination()
    println("*** streaming terminated")
  } catch {
    case e: Exception => {
      println("*** streaming exception caught in monitor thread")
    }
  }

  println("*** started streaming context")
}
