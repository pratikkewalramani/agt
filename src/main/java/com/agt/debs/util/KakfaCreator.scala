package com.agt.debs
import java.util.Properties

import kafka.admin.AdminUtils
import kafka.utils.{ZkUtils}
/**
  * Created by pratik_k on 11/27/2017.
  */
object KafkaCreator{
  val sessionTimeoutMs = 10000
  val connectionTimeoutMs = 10000
  val (zkClient, zkConnection) = ZkUtils.createZkClientAndConnection(
    "localhost:2181", sessionTimeoutMs, connectionTimeoutMs)
  val zkUtils = new ZkUtils(zkClient, zkConnection, false)
  val numPartitions = 4
  val replicationFactor = 1
  val topicConfig = new Properties

  def createTopic(topic:String): Unit = {
    if(!AdminUtils.topicExists(zkUtils, topic))
      AdminUtils.createTopic(zkUtils, topic, numPartitions, replicationFactor, topicConfig)
  }
}
