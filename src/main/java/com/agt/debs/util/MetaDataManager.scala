package com.agt.debs.util

import scala.collection.mutable

/**
  * Created by pratik_k on 11/16/2017.
  */
object MetaDataManager {
  var machineMetaDataMap: mutable.HashMap[String, String] = mutable.HashMap.empty
  var dimensionMetaDataMap: mutable.HashMap[String, Tuple2[Option[Double], Option[Int]]] = mutable.HashMap.empty
  var listOfMachines:mutable.Set[String] = mutable.HashSet.empty
}
