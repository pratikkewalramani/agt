package com.agt.debs.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Contains methods to parse a string to {@link LocalDate} and vice-versa
 * @author pratik_k
 *
 */
public class DateConverter {
	private final static Logger log = LoggerFactory.getLogger(DateConverter.class);
	private static DateTimeFormatter formatter = DateTimeFormatter.ISO_ZONED_DATE_TIME; 
	
	/**
	 * Converts the date in string format to {@link LocalDate} format  
	 * @param date the date in string format
	 * @return {@link LocalDate} object 
	 */
	public static LocalDateTime parse(String date) {
		try {
			return LocalDateTime.parse(date,formatter);
		} catch(DateTimeParseException ex) {
			log.error(ex.toString());
			return null;
		}
	}

	/**
	 * Converts the date from {@link LocalDate} to string format.  
	 * @param date {@link LocalDate} object 
	 * @return string form of the date
	 */
	public static String print(LocalDateTime date) {
		return date.format(formatter);
	}
}
