package com.agt.debs

import com.agt.debs.model.SystemParameters
import com.agt.debs.processing.SparkStreamer
import com.agt.debs.util.{EmbeddedKafkaServer, MetaDataManager}
import org.apache.jena.query.{QuerySolution, ResultSetFormatter, _}
import org.apache.jena.rdf.model.{Model, RDFNode}
import org.apache.jena.riot.RDFDataMgr

/**
  * Created by pratik_k on 11/28/2017.
  */
object Initializer {
  val kafkaServer = new EmbeddedKafkaServer()

  var debsGcBenchmarkSystem: DebsGcBenchmarkSystem = null

  def init(debsSystem: DebsGcBenchmarkSystem): Unit = {
    debsGcBenchmarkSystem = debsSystem
    kafkaServer.start()
    readMetaData()
    new SparkStreamer(kafkaServer)
  }

  def init(): Unit = {
    kafkaServer.start()
    SystemParameters.setParameters(50, 50, 5)
    readMetaData()
    new SparkStreamer(kafkaServer)
  }

  def readMetaData() = {
    val prefixMetadata: String = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>" +
      "PREFIX IoTCore: <http://www.agtinternational.com/ontologies/IoTCore#>" +
      "PREFIX ssn: <http://purl.oclc.org/NET/ssnx/ssn#>" +
      "PREFIX ns2: <http://www.agtinternational.com/ontologies/WeidmullerMetadata#>"

    val queryThreshold: String = prefixMetadata + "SELECT ?dimension ?threshold WHERE { " +
      "?probabilityThreshold ns2:isThresholdForProperty ?dimension ." +
      "?probabilityThreshold IoTCore:valueLiteral ?threshold ." +
      "}"

    val queryNoOfClusters: String = prefixMetadata + "SELECT ?dimension ?noCluster WHERE { " +
      "?dimension ns2:hasNumberOfClusters ?noCluster ." +
      "}"

    val queryMachineDimension: String = prefixMetadata + "SELECT ?machine ?dimension WHERE { " +
      "?machine IoTCore:hasModel ?machineType ." +
      "?machineType ssn:hasProperty ?dimension ." +
      "}"

    val modelMetaData: Model =
      RDFDataMgr.loadModel(this.getClass().getResource("/molding_machine_10M.metadata.nt").getFile)
    val queryDimensionMetadata: Query =
      QueryFactory.create(queryMachineDimension)
    val queryThresholdMetadata: Query = QueryFactory.create(queryThreshold)
    val queryClusterMetadata: Query = QueryFactory.create(queryNoOfClusters)
    val qexecDimensionMeta: QueryExecution =
      QueryExecutionFactory.create(queryDimensionMetadata, modelMetaData)
    val qexecThresholdMeta: QueryExecution =
      QueryExecutionFactory.create(queryThresholdMetadata, modelMetaData)
    val qexecClusterMeta: QueryExecution =
      QueryExecutionFactory.create(queryClusterMetadata, modelMetaData)

    try {
      val results: ResultSet = qexecDimensionMeta.execSelect()
      //ResultSetFormatter.out(System.out, results)
      while (results.hasNext) {
        val soln: QuerySolution = results.nextSolution()
        val machine: RDFNode = soln.getResource("machine")
        val dimension: RDFNode = soln.getResource("dimension")
        val idArr: Array[String] = machine.toString.split("#")
        val dimArr: Array[String] = dimension.toString.split("#")
        val machineId = idArr(1)
        val dimenstionId = dimArr(1)
        if(!MetaDataManager.listOfMachines.contains(machineId)) {
          MetaDataManager.listOfMachines.+=(machineId)
          kafkaServer.createTopic(machineId)
        }
        if(!MetaDataManager.machineMetaDataMap.contains(machineId)){
          MetaDataManager.machineMetaDataMap.put(machineId, dimenstionId)
        }
      }
    } catch {
      case e: Exception => e.printStackTrace()
    } finally qexecDimensionMeta.close()

    try {
      val results: ResultSet = qexecThresholdMeta.execSelect()
      //ResultSetFormatter.out(System.out, results)
      while (results.hasNext) {
        val soln: QuerySolution = results.nextSolution()
        val dimension: RDFNode = soln.getResource("dimension")
        val threshold: RDFNode = soln.getLiteral("threshold")
        val dimArr: Array[String] = dimension.toString.split("#")
        val thresholdValue = threshold.asLiteral().getDouble
        val dimensionId = dimArr(1)
        val tuple = MetaDataManager.dimensionMetaDataMap.getOrElseUpdate(dimensionId, (Some(thresholdValue), None))
        MetaDataManager.dimensionMetaDataMap.put(dimensionId, (Some(thresholdValue), tuple._2))
      }
    } catch {
      case e: Exception => e.printStackTrace()
    } finally qexecThresholdMeta.close()

    try {
      val results: ResultSet = qexecClusterMeta.execSelect()
      //ResultSetFormatter.out(System.out, results)
      while (results.hasNext) {
        val soln: QuerySolution = results.nextSolution()
        val dimension: RDFNode = soln.getResource("dimension")
        val noOfCluster: RDFNode = soln.getLiteral("noCluster")
        val dimArr: Array[String] = dimension.toString.split("#")
        val clusterValue = noOfCluster.asLiteral().getInt
        val dimensionId = dimArr(1)
        val tuple = MetaDataManager.dimensionMetaDataMap.getOrElseUpdate(dimensionId, (None, Some(clusterValue)))
        MetaDataManager.dimensionMetaDataMap.put(dimensionId, (tuple._1, Some(clusterValue)))
      }
    } catch {
      case e: Exception => e.printStackTrace()
    } finally qexecThresholdMeta.close()
  }
}
