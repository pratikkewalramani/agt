package com.agt.debs;

import com.agt.debs.rdf.RDFFormator;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.HashMap;
import java.util.Map;

import static com.agt.debs.DebsGcBenchmarkSystem.MAX_CLUSTER_ITERATIONS_INPUT_NAME;
import static com.agt.debs.DebsGcBenchmarkSystem.TRANSITIONS_COUNT_INPUT_NAME;
import static com.agt.debs.DebsGcBenchmarkSystem.WINDOW_SIZE_INPUT_NAME;
import static org.hobbit.core.Constants.SYSTEM_PARAMETERS_MODEL_KEY;

public class DebsGcBenchmarkSystemRunner {
	private static final Logger logger = LoggerFactory.getLogger(DebsGcBenchmarkSystemRunner.class);

    public static void main(String... args) throws Exception {
        logger.debug("Running...");
        DebsGcBenchmarkSystem system = null;
        try {
            Map<String, Object> params = new HashMap<>();
            params.put(MAX_CLUSTER_ITERATIONS_INPUT_NAME, 50);
            params.put(TRANSITIONS_COUNT_INPUT_NAME, 5);
            params.put(WINDOW_SIZE_INPUT_NAME, 10);
            try {
                String parameterModel = System.getenv().get(SYSTEM_PARAMETERS_MODEL_KEY);
                logger.debug("Params from platform", parameterModel);
                Model model = RDFDataMgr.loadModel(parameterModel);
                params = RDFFormator.getKeyValue(model);
            }
            catch (Exception e){
                logger.debug("No parameters passed to the system.");
            }

            system = new DebsGcBenchmarkSystem(params);
            Initializer.init(system);
            system.init();
            system.run();
        } finally {
            if (system != null) {
                system.close();
            }
        }
        logger.debug("Finished.");
    }
}
